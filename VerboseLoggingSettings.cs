using System.Collections.Generic;
using UnityEngine;

namespace RobinBird.Logging.Unity
{
	[CreateAssetMenu(fileName = "VerboseLoggingSettings.asset", menuName = "Logging/VerboseLoggingSettings")]
	public class VerboseLoggingSettings : ScriptableObject, IVerboseLoggingSettings
	{
		public static VerboseLoggingSettings Instance =>
			Resources.Load<VerboseLoggingSettings>("VerboseLoggingSettings");

		[SerializeField]
		private bool allowAll;

		[SerializeField]
		private List<string> verboseFilePaths;

		[SerializeField]
		private List<string> verboseCategories;

		[SerializeField]
		private List<string> verboseMembers;


		public bool AllowAll => allowAll;
		public ICollection<string> VerboseFilePaths => verboseFilePaths;
		public ICollection<string> VerboseCategories => verboseCategories;
		public ICollection<string> VerboseMembers => verboseMembers;
	}
}